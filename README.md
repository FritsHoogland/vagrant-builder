This is version 2 of vagrant builder, a scriptset to build virtual machines with linux and the oracle database. This can be any version of the Oracle database starting from 11.2.0.1, and any version of the grid infrastructure starting from 11.2.0.4.

Usage of this software and methods are strictly at your own risk, no warranties in any way are given.

1. Build a virtual machine using vagrant  

    a. I assume you understand you need to have vagrant and virtualbox installed.  

    b. You have cloned https://gitlab.com/FritsHoogland/vagrant-builder.git already, otherwise you couldn't read this, unless you are reading it in gitlab.  
   `git clone https://gitlab.com/FritsHoogland/vagrant-builder.git your-vm-name`

    c. Open the Vagrantfile (readwrite)

    d. Set (mandatory):

     - hostonly_network_ip_address
     - mos_username
     - mos_password
     - database_name (if you leave it empty, no database will be created)
     - asm_version (if you leave it empty, no single instance clusterware will be installed; if a version is set, the database will be created in ASM)
     - database_version (if you leave it empty, no database software will be installed)  

    e. Set (optional):

     - global_password (this is the password that is set for all database accounts; if you move your creation to production, make sure to change the passwords)
     - filesystem (if you want the /u01 database partition and optionally the database to run on another filesystem than xfs)
     - linux (the default is the latest oracle linux 7 bento box)
     - vm_cpus 
     - vm_memory
     - vm_hostname
     - database_characterset
     - redologfile_size
     - pluggable_database
     - sga_target_mb
     - pga_aggregate_target_mb
     - asm_disdiscoverystring_without_asterisk (allows you to choose between udev or asmlib when asm_version is set; please mind asmlib requires oracleasmlib-2.0.12-1.el6.x86_64.rpm to be downloaded from OTN and placed in the files/ directory)  

    f.  Database software; important!

     - vagrant-builder can obtain the installation sources as well as any needed patches from 3 locations, in this order, for any required file:
       - Fastest: use the vboxsf mount. put one or more of the files in the directory where the Vagrantfile is. This requires the virtualbox additions to be usable in the VM. The file will be unzipped directly from the mount.
       - Less fast: put one or more files in the in the 'files' directory. These will be copied into the VM to /tmp for installation.
       - Slowest: do not put the files anywhere. If possible, the file will be downloaded from oracle as patch.
     - For database and clusterware versions that are downloadable as patches (11.2.0.2, 11.2.0.3, 11.2.0.4, 12.1.0.2), vagrant-builder will download the installation source (which are patches) directly from Oracle, unless the installation source is found in the vboxsf mount or /tmp.
    - For database and clusterware versions that are not downloadable as patches (11.2.0.1, 12.1.0.1, 12.2.0.1, 18, 19), you MUST provide the installation source in the directory where the Vagrantfile is or in the files directory. These must be the downloads from https://www.oracle.com/database/technologies/oracle-database-software-downloads.html for 11.2.0.1, 12.2.0.1, 18 and 19, and the download from https://edelivery.oracle.com for 12.1.0.1.
    - All the other patch files that are used, opatch and the patches for the specific selected PSU versions, are downloaded directly from Oracle, unless they are found in the vboxsf mount or /tmp.  

    g. Execute vagrant to build the VM with the Oracle database install:  
      `vagrant up`

    h. Vagrant file creates the VM using virtualbox, install git and ansible, copy files in the files directory to /tmp in the VM, git clone https://gitlab.com/FritsHoogland/oracle-database-setup.git and then run ~vagrant/oracle-database-setup/setup.yml in the VM. If the VM already exists, it will validate if the provisioning has run, and if so, then just start the VM. 

    i. If an issue happened during running ansible to install oracle, this always can be found and reviewed in the ansible logfile: ~vagrant/oracle-database-setup/ansible.log in the virtual machine (vagrant ssh to log into the VM). The setup can be run again by executing setup.yml.
